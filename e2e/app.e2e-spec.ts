import { StatCardPage } from './app.po';

describe('stat-card App', () => {
  let page: StatCardPage;

  beforeEach(() => {
    page = new StatCardPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
