import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Player } from '../player';

@Injectable()
export class GetPlayerService {

  private static readonly _url :string = './player-stats.json';

  constructor(private _http: Http) {

  }

  getPlayer(): Observable<Player[]> {
   return this._http.get(GetPlayerService._url).map(res => res.json().players as Player[]).map((data: Array<any>) => {
      const players: Array<Player> = [];
      let player: Player;

      if (data) {
        data.forEach((row) => {
          player = new Player;
          player.id = row.player.id;
          player.name = row.player.name.first + ' ' + row.player.name.last;
          player.positionInfo = row.player.info.positionInfo;

          let stats = [];
          row.stats.forEach(stat => {
            stats[stat.name] = stat.value;
          });
          player.stats = stats;

          players.push(player);
        });
      }

      return players;
    });
  }

}
