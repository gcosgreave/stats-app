import { TestBed, inject } from '@angular/core/testing';

import { GetPlayerService } from './get-player.service';

describe('GetPlayerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetPlayerService]
    });
  });

  it('should ...', inject([GetPlayerService], (service: GetPlayerService) => {
    expect(service).toBeTruthy();
  }));
});
