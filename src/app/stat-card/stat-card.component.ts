import { Component, OnInit } from '@angular/core';
import { GetPlayerService } from '../get-player.service';
import { Player } from '../../player';

@Component({
  selector: 'stat-card',
  templateUrl: './stat-card.component.html',
  styleUrls: ['./stat-card.component.css']
})

export class StatCardComponent implements OnInit {
  public players: Player[];
  public player: Player;

  /**
   * Constructor
   *
   * @param GetPlayerService
   */
  constructor(private GetPlayerService: GetPlayerService) { }

  /**
   * Initialise
   */
  ngOnInit() {
    this.GetPlayerService.getPlayer().subscribe(players => {
      this.players = players;
      this.selectFirstPlayer(players);
    });
  }

  /**
   * Select the first player initially
   * @param players
   */
  private selectFirstPlayer(players: Player[]) {
    // Exit if there are no players available
    if (players.length < 1) {
      // TODO: Players unavailable handler..
      return false;
    }
    this.player = this.players[0];
  }

  /**
   * Update the selected player object
   * @param selectedPlayer Player
   */
  updateSelectedPlayer(selectedPlayer: Player) {
    this.player = selectedPlayer;
  }

}
