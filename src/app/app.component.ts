import { Component } from '@angular/core';
import { HttpModule } from '@angular/http';
import { GetPlayerService } from './get-player.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [GetPlayerService, HttpModule]
})
export class AppComponent {
  title = 'Select Premier League Player';
}
