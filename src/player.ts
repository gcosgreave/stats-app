export class Player {
    id: number;
    name: string;
    positionInfo: number;
    stats: Array<any>;

    public get picture(): string
    {
        return 'p' + this.id + '.png';
    }

    /**
     * Get the number of goals / match
     *
     * @returns {number}
     */
    public get goalsPerMatch(): number
    {
        if (this.stats['goals'] && this.stats['appearances']) {
            return (this.stats['goals'] / this.stats['appearances']);
        }

        return 0;
    }

    /**
     * Get the number of passes / match
     *
     * @returns {number}
     */
    public get passesPerMinute(): number
    {
        if (this.stats['fwd_pass'] && this.stats['backward_pass'] && this.stats['mins_played']) {
           return ((this.stats['fwd_pass'] + this.stats['backward_pass']) / this.stats['mins_played']);
        }

        return 0;
    }
}
